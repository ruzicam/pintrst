namespace Pintrst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        CommentText = c.String(),
                        Pin_PinID = c.Int(),
                        User_UserID = c.Int(),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Pins", t => t.Pin_PinID)
                .ForeignKey("dbo.Users", t => t.User_UserID)
                .Index(t => t.Pin_PinID)
                .Index(t => t.User_UserID);
            
            CreateTable(
                "dbo.Pins",
                c => new
                    {
                        PinID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 60),
                        Text = c.String(maxLength: 320),
                        PinPicture = c.String(nullable: false),
                        Tag = c.String(nullable: false, maxLength: 30),
                        User_UserID = c.Int(),
                    })
                .PrimaryKey(t => t.PinID)
                .ForeignKey("dbo.Users", t => t.User_UserID)
                .Index(t => t.User_UserID);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        LikeID = c.Int(nullable: false, identity: true),
                        Value = c.Boolean(nullable: false),
                        Pin_PinID = c.Int(),
                        User_UserID = c.Int(),
                    })
                .PrimaryKey(t => t.LikeID)
                .ForeignKey("dbo.Pins", t => t.Pin_PinID)
                .ForeignKey("dbo.Users", t => t.User_UserID)
                .Index(t => t.Pin_PinID)
                .Index(t => t.User_UserID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Nickname = c.String(),
                        Mail = c.String(),
                        Location = c.String(),
                        Picture = c.String(),
                        Pin_PinID = c.Int(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.Pins", t => t.Pin_PinID)
                .Index(t => t.Pin_PinID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "User_UserID", "dbo.Users");
            DropForeignKey("dbo.Users", "Pin_PinID", "dbo.Pins");
            DropForeignKey("dbo.Pins", "User_UserID", "dbo.Users");
            DropForeignKey("dbo.Likes", "User_UserID", "dbo.Users");
            DropForeignKey("dbo.Likes", "Pin_PinID", "dbo.Pins");
            DropForeignKey("dbo.Comments", "Pin_PinID", "dbo.Pins");
            DropIndex("dbo.Users", new[] { "Pin_PinID" });
            DropIndex("dbo.Likes", new[] { "User_UserID" });
            DropIndex("dbo.Likes", new[] { "Pin_PinID" });
            DropIndex("dbo.Pins", new[] { "User_UserID" });
            DropIndex("dbo.Comments", new[] { "User_UserID" });
            DropIndex("dbo.Comments", new[] { "Pin_PinID" });
            DropTable("dbo.Users");
            DropTable("dbo.Likes");
            DropTable("dbo.Pins");
            DropTable("dbo.Comments");
        }
    }
}
