﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Pintrst.Models
{
    public class Pin
    {
        public int PinID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 2)]
        public string Title { get; set; }

        [StringLength(320, MinimumLength = 10)]
        public string Text { get; set; }

        [Required]
        public string PinPicture { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string Tag { get; set; }

        public virtual User User { get; set; }
        public virtual List<User> Users { get; set; }
        public virtual List<Comment> Comments { get; set; }
        //public virtual List<Tag> Tags { get; set; }
        public virtual List<Like> Likes { get; set; }
    }

    public class PinDBContext : DbContext
    {
        public DbSet<Pin> Pins { get; set; }

        public System.Data.Entity.DbSet<Pintrst.Models.Comment> Comments { get; set; }
    }
}