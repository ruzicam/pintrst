﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pintrst.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        public string Hashtag { get; set; }

        public virtual List<Pin> Pins { get; set; }
    }
}