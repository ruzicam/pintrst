﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pintrst.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Nickname { get; set; }
        public string Mail { get; set; }
        public string Location { get; set; }
        public string Picture { get; set; }

        public virtual List<Like> Likes { get; set; }
    }
}