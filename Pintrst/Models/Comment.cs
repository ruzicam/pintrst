﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Pintrst.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public string CommentText { get; set; }
        
        public virtual User User { get; set; }
        public virtual Pin Pin { get; set; }
    }

    public class CommentDBContext : DbContext
    {
        public DbSet<Comment> Comments { get; set; }
    }
}