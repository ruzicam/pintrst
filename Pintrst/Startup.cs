﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pintrst.Startup))]
namespace Pintrst
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
